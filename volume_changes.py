
from collections import OrderedDict
import csv
from datetime import datetime, timedelta
import pickle
from pprint import pprint as pp

import matplotlib.pyplot as plt
import psycopg2 as pg

from webbrowser import open_new_tab

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import (conf, db, location_sources as locsrcs, utils)

RUGBY_REGIONS = [conf.WHANGAREI_CITY_ID, conf.AUCKLAND_CITY_ID,
    conf.HAMILTON_CITY_ID, conf.ROTORUA_CITY_ID, conf.WELLINGTON_CITY_ID,
    conf.CHRISTCHURCH_CITY_ID, conf.DUNEDIN_CITY_ID]
region_names = {
    conf.WHANGAREI_CITY_ID: 'Whangarei City',
    conf.AUCKLAND_CITY_ID: 'Auckland City',
    conf.HAMILTON_CITY_ID: 'Hamilton City',
    conf.ROTORUA_CITY_ID: 'Rotorua City',
    conf.WELLINGTON_CITY_ID: 'Wellington City',
    conf.CHRISTCHURCH_CITY_ID: 'Christchurch City',
    conf.DUNEDIN_CITY_ID: 'Dunedin City',
}
regname_exclusions = {
    'Whangarei City': 'Northland RTO (ex-Whangarei)',
    'Auckland City': 'Auckland RTO (ex-centre isthmus)',
    'Hamilton City': 'Waikato RTO (ex-Hamilton)',
    'Rotorua City': 'Rotorua RTO (ex-Rotorua City)',
    'Wellington City': 'Wellington RTO (ex-Wellington City)',
    'Christchurch City': 'Canterbury RTO (ex-Christchurch)',
    'Dunedin City': 'Dunedin RTO (ex-Dunedin City)',
}
region_set_2_regnames = ['Auckland City', 'Auckland RTO (ex-centre isthmus)',
    'Bay of Plenty RTO', 'Canterbury RTO (ex-Christchurch)', 'Central Otago RTO',
    'Christchurch City', 'Clutha RTO', 'Coromandel RTO', 'Dunedin City',
    'Dunedin RTO (ex-Dunedin City)', 'Fiordland RTO', 'Gisborne RTO',
    'Hamilton City', 'Hawkes Bay RTO', 'Kaikoura RTO', 'Kapiti-Horowhenua RTO',
    'Kawerau-Whakatane RTO', 'Lake Taupo RTO', 'Lake Wanaka RTO',
    'Manawatu RTO', 'Marlborough RTO', 'Nelson Tasman RTO',
    'Northland RTO (ex-Whangarei)', 'Queenstown RTO', 'Rotorua City',
    'Rotorua RTO (ex-Rotorua City)', 'Ruapehu RTO', 'Southland RTO',
    'Taranaki RTO', 'Waikato RTO (ex-Hamilton)', 'Wairarapa RTO', 'Waitaki RTO',
    'Wanganui RTO', 'Wellington City', 'Wellington RTO (ex-Wellington City)',
    'West Coast RTO', 'Whangarei City']
REGIONAL_DATA_QUICK = 'regional_data_quick.pkl'
REGIONAL_DATA_DETAILED = 'regional_data_detailed.pkl'
VISITS_CSV = 'lions_visits.csv'

def add_special_regions(cur_local):
    locsrcs.RegionTables.add_custom_region_from_shp(cur_local,
        region_id=conf.WHANGAREI_CITY_ID, label=region_names[conf.WHANGAREI_CITY_ID],
        tblname='whangarei_combined')
    locsrcs.RegionTables.add_custom_region_from_shp(cur_local,
        region_id=conf.AUCKLAND_CITY_ID, label=region_names[conf.AUCKLAND_CITY_ID],
        tblname='auckland_city_combined')
    locsrcs.RegionTables.add_custom_region_from_shp(cur_local,
        region_id=conf.HAMILTON_CITY_ID, label=region_names[conf.HAMILTON_CITY_ID],
        tblname='hamilton_city_combined')
    locsrcs.RegionTables.add_custom_region_from_shp(cur_local,
        region_id=conf.ROTORUA_CITY_ID, label=region_names[conf.ROTORUA_CITY_ID],
        tblname='rotorua_city_combined')
    locsrcs.RegionTables.add_custom_region_from_shp(cur_local,
        region_id=conf.WELLINGTON_CITY_ID, label=region_names[conf.WELLINGTON_CITY_ID],
        tblname='wellington_combined')
    locsrcs.RegionTables.add_custom_region_from_shp(cur_local,
        region_id=conf.CHRISTCHURCH_CITY_ID, label=region_names[conf.CHRISTCHURCH_CITY_ID],
        tblname='christchurch_city_combined')
    locsrcs.RegionTables.add_custom_region_from_shp(cur_local,
        region_id=conf.DUNEDIN_CITY_ID, label=region_names[conf.DUNEDIN_CITY_ID],
        tblname='dunedin_city_combined')
    con_rem, cur_rem = db.Pg.get_rem()
    region_id_clause = db.Pg.nums2clause(RUGBY_REGIONS)
    sql_set_region_set_id = """\
    UPDATE {region}
    SET region_set_id = 2
    WHERE region_id IN {region_id_clause}
    """.format(region=conf.REGION, region_id_clause=region_id_clause)
    cur_rem.execute(sql_set_region_set_id)
    con_rem.commit()

def get_daily_freqs(region_id, start_date_str='2017-05-01',
        end_date_str='2017-07-07'):
    debug = True
    unused_con_rem, cur_rem = db.Pg.get_rem()
    on_dt = datetime.strptime(start_date_str, '%Y-%m-%d')
    to_dt = datetime.strptime(end_date_str, '%Y-%m-%d')
    
    """
    lbs_agg.fact_rti_summary supplies IMSI_rti, cell_id_rti
    lbs_raw.dim_subscriber supplies mccmnc thus country - link using IMSI_rti
    """
    sql_tpl = """\
    SELECT
    origin,
      COUNT(*) AS
    freq
    
    FROM (

      SELECT DISTINCT origin, IMSI_rti

      FROM (
    
        SELECT IMSI_rti,
        origin,
        grid12
        FROM (
    
          SELECT
          connections.IMSI_rti,
          cell_id_rti, 
            CASE SUBSTRING(mccmnc,1,3)
              WHEN 530 THEN 'Domestic'
              WHEN 234 THEN 'United Kingdom'
              WHEN 505 THEN 'Australia'
              WHEN 204 THEN 'Netherlands'
              WHEN 460 THEN 'China'
              ELSE 'Other International'
            END AS
          origin
          FROM (
            SELECT DISTINCT IMSI_rti, cell_id_rti FROM
            lbs_agg.fact_rti_summary
            WHERE date = %s
          ) AS connections 
          INNER JOIN
          lbs_raw.dim_subscriber
          USING(IMSI_rti)

        ) AS imsi_origin
        INNER JOIN
        master.cell_location AS locs
        USING(cell_id_rti)
        WHERE locs.start_date <= %s  -- getting location as at date
          AND (
            (end_date IS NULL) 
            OR
            (end_date >= %s)
          )      

      ) AS imsi_origin_grid12

      INNER JOIN

      master.region_grid12
      USING(grid12)
      WHERE region_id = %s

    ) AS imsi_origin
    GROUP BY origin
    ORDER BY origin
    """
    daily_freqs = []
    while on_dt <= to_dt:
        on_date_str = on_dt.strftime('%Y-%m-%d')
        cur_rem.execute(sql_tpl, (on_date_str, on_date_str, on_date_str,
            region_id))
        data = cur_rem.fetchall()
        daily_data = {'date': on_date_str, }
        for resident_flag, freq in data:
            daily_data[resident_flag] = freq
        daily_freqs.append(daily_data)
        if debug: print(on_date_str, data)
        on_dt += timedelta(days=1)
    return daily_freqs

def get_region_data():
    debug = True
    region_daily_dets = {}
    ranges = [
#         ('2017-05-01', '2017-07-07'),
#         ('2016-05-01', '2016-07-07'),
#         ('2015-05-01', '2015-07-07'),
        ('2017-01-01', '2017-07-09'),
        ('2016-01-01', '2016-12-31'),
        ('2015-01-01', '2015-12-31'),
    ]
    for region_id in RUGBY_REGIONS:
        years_daily_dets = []
        for start, end in ranges: 
            daily_freqs = get_daily_freqs(region_id, start_date_str=start,
                end_date_str=end)
            years_daily_dets.append(daily_freqs)
        region_daily_dets[region_id] = years_daily_dets
    if debug: pp(region_daily_dets)
    with open(REGIONAL_DATA_DETAILED, 'wb') as f:
        pickle.dump(region_daily_dets, f)

def colours():
    colours = [
        'red',
        'deepskyblue',
        'darkgreen',
        'mediumpurple',
        'gold',
        'dimgray',
        'teal',
        'lime',
        'deeppink',
        'black',
        'chocolate',
        'darkorange',
        'silver',
        'purple',
        'slategrey',
        'rosybrown',
        'blue',
        'olive',
        #'deepskyblue',
        'palegoldenrod',
        'darkslateblue',
        'blanchedalmond',
        #'mediumpurple',
        'lightskyblue',
        'darkolivegreen',
        'khaki',
        'lightblue',
        'yellowgreen',
        #'lime',
        'peachpuff',
        'lawngreen',
        'darkgray',
        'lightgoldenrodyellow',
        'darkmagenta',
        #'black',
        'sienna',
        'lemonchiffon',
        'palevioletred',
        'blueviolet',
        'mediumslateblue',
        'olivedrab',
        'darkcyan',
        'fuchsia',
        'white',
        'slateblue',
        'darkkhaki',
        'snow',
        'burlywood',
        #'silver',
        'coral',
        'darkseagreen',
        'crimson',
        'linen',
        'mintcream',
        'darkviolet',
        'beige',
        'mediumaquamarine',
        'forestgreen',
        'seashell',
        'peru',
        'saddlebrown',
        'darkslategrey',
        #'gold',
        'aquamarine',
        'yellow',
        'lightgray',
        'lightsteelblue',
        'goldenrod',
        'cornflowerblue',
        'greenyellow',
        'antiquewhite',
        'darkgoldenrod',
        'honeydew',
        'papayawhip',
        'darkturquoise',
        'turquoise',
        'lightslategrey',
        'darkgrey',
        'orchid',
        'green',
        #'slategrey',
        #'rosybrown',
        'ghostwhite',
        'navy',
        #'darkgreen',
        'cadetblue',
        'powderblue',
        'dodgerblue',
        'cyan',
        'plum',
        'indianred',
        'lightcyan',
        'gray',
        'palegreen',
        'maroon',
        'aliceblue',
        'oldlace',
        'mediumseagreen',
        'lightslategray',
        'thistle',
        'mediumorchid',
        'salmon',
        'skyblue',
        'paleturquoise',
        'bisque',
        'mediumvioletred',
        'seagreen',
        'lightgrey',
        'pink',
        'orangered',
        'floralwhite',
        'lavenderblush',
        'indigo',
        'lightgreen',
        #'chocolate',
        'lightpink',
        'steelblue',
        'mediumblue',
        'darkblue',
        'moccasin',
        'springgreen',
        'slategray',
        'aqua',
        'magenta',
        'lavender',
        'mediumturquoise',
        'cornsilk',
        'darkslategray',
        #'darkorange',
        'sandybrown',
        'dimgrey',
        'tomato',
        'azure',
        'navajowhite',
        #'blue',
        'darksalmon',
        'midnightblue',
        'limegreen',
        #'purple',
        'mistyrose',
        #'teal',
        'violet',
        'tan',
        'chartreuse',
        'mediumspringgreen',
        'orange',
        'lightcoral',
        #'olive',
        'wheat',
        'ivory',
        'royalblue',
        #'red',
        #'deeppink',
        'darkorchid',
        'hotpink',
        'grey',
        'firebrick',
        'lightsalmon',
        'lightseagreen',
        'whitesmoke',
        'lightyellow',
        'darkred',
        'gainsboro',
        'brown',
    ]
    for colour in colours:
        yield colour

def add_rugby_region_set():
    con_rem, cur_rem = db.Pg.get_rem()
    sql_insert = """\
    INSERT INTO {region_set}
    (label, description, notes, eff_start_dt, eff_end_dt)
    VALUES ('Lions Tour Regions',
      'NZ Lions Tour 2017 Regions',
      'Uses RTO boundaries with game cities removed as separate regions',
      null,
      null
    )
    """.format(region_set=conf.REGION_SET)
    cur_rem.execute(sql_insert)
    con_rem.commit()

def reuse_region(old_region_id, new_region_id):
    con_rem, cur_rem = db.Pg.get_rem()
    sql_old_dets_tpl = """\
    SELECT * FROM {region} WHERE region_id = %s
    """.format(region=conf.REGION)
    cur_rem.execute(sql_old_dets_tpl, (old_region_id, ))
    row = cur_rem.fetchone()
    sql_insert_tpl = """\
    INSERT INTO {region}
    (region_id, region_set_id, label, notes, geom, eff_start_dt, eff_end_dt)
    VALUES (%s, %s, %s, %s, %s, %s, %s)
    """.format(region=conf.REGION)
    cur_rem.execute(sql_insert_tpl, (new_region_id, 2,
        row['label'], row['notes'], row['geom'], row['eff_start_dt'],
        row['eff_end_dt']))
    con_rem.commit()

def reuse_old_regions():
    old2new = [
        (conf.COROMANDEL_RTO_ID, conf.COROMANDEL_ID),
        (conf.BOP_RTO_ID, conf.BOP_ID),
        (conf.KAWERAU_RTO_ID, conf.KAWERAU_WHAKATANE_ID),
        (conf.GISBORNE_RTO_ID, conf.GISBORNE_ID),
        (conf.TARANAKI_RTO_ID, conf.TARANAKI_ID),
        (conf.RUAPEHU_RTO_ID, conf.RUAPEHU_ID),
        (conf.TAUPO_RTO_ID, conf.LAKE_TAUPO_ID),
        (conf.HAWKES_BAY_RTO_ID, conf.HAWKES_BAY_ID),
        (conf.WANGANUI_RTO_ID, conf.WANGANUI_ID),
        (conf.MANAWATU_RTO_ID, conf.MANAWATU_ID),
        (conf.KAPITI_RTO_ID, conf.KAPITI_ID),
        (conf.WAIRARAPA_RTO_ID, conf.WAIRARAPA_ID),
        (conf.NELSON_RTO_ID, conf.NELSON_ID),
        (conf.MARLBOROUGH_RTO_ID, conf.MARLBOROUGH_ID),
        (conf.KAIKOURA_RTO_ID, conf.KAIKOURA_ID),
        (conf.WEST_COAST_RTO_ID, conf.WEST_COAST_ID),
        (conf.FIORDLAND_RTO_ID, conf.FIORDLAND_ID),
        (conf.QUEENSTOWN_RTO_ID, conf.QUEENSTOWN_ID),
        (conf.LAKE_WANAKA_RTO_ID, conf.LAKE_WANAKA_ID),
        (conf.CENTRAL_OTAGO_RTO_ID, conf.CENTRAL_OTAGO_ID),
        (conf.WAITAKI_RTO_ID, conf.WAITAKI_ID),
        (conf.SOUTHLAND_RTO_ID, conf.SOUTHLAND_ID),
        (conf.CLUTHA_RTO_ID, conf.CLUTHA_ID),
    ]
    for old_region_id, new_region_id in old2new:
        reuse_region(old_region_id, new_region_id)

def add_complementary_regions():
    complementary_dets = [
        (conf.NORTHLAND_LESS_WHANGAREI_ID, 'Northland RTO (ex-Whangarei)',
            conf.NORTHLAND_RTO_ID, conf.WHANGAREI_CITY_ID),
        (conf.AUCKLAND_LESS_CENTRE_ID, 'Auckland RTO (ex-centre isthmus)',
            conf.AUCK_RTO_ID, conf.AUCKLAND_CITY_ID),
        (conf.WAIKATO_LESS_HAMILTON_ID, 'Waikato RTO (ex-Hamilton)',
            conf.WAIKATO_RTO_ID, conf.HAMILTON_CITY_ID),
        (conf.ROTORUA_LESS_CITY_ID, 'Rotorua RTO (ex-Rotorua City)',
            conf.ROTORUA_RTO_ID, conf.ROTORUA_CITY_ID),
        (conf.WELLINGTON_LESS_CITY_ID, 'Wellington RTO (ex-Wellington City)',
            conf.WELLINGTON_RTO_ID, conf.WELLINGTON_CITY_ID),
        (conf.CANT_LESS_CITY_ID, 'Canterbury RTO (ex-Christchurch)',
            conf.CANTERBURY_RTO_ID, conf.CHRISTCHURCH_CITY_ID),
        (conf.DUNEDIN_LESS_CITY_ID, 'Dunedin RTO (ex-Dunedin City)',
            conf.DUNEDIN_RTO_ID, conf.DUNEDIN_CITY_ID),
    ]
    for (complementary_region_id, complementary_region_label,
            large_region_id, region_id_to_subtract) in complementary_dets:
        locsrcs.RegionTables.make_complementary_region(complementary_region_id,
            complementary_region_label, 2, large_region_id,
            region_id_to_subtract)
    
def make_lions_poi_imsis_tbl(start_date_str, end_date_str):
    con_rem, cur_rem = db.Pg.get_rem()
    db.Pg.drop_tbl(con_rem, cur_rem, tbl=conf.LION_POI_IMSIS)
    sql = """\
    SELECT *
    INTO {lion_poi_imsis}
    FROM (
      SELECT DISTINCT IMSI_rti
      FROM {connections}
      INNER JOIN
      {cell_info}
      USING(cell_id_rti)
      WHERE date BETWEEN %s AND %s
      AND {cell_info}.start_date <= date
      AND (
        ({cell_info}.end_date IS NULL)
        OR
        ({cell_info}.end_date >= date)
      )
      AND grid12 IN (
        SELECT grid12
        FROM {region_grid12}
        WHERE region_id BETWEEN 50 AND 56 -- WHANGAREI_CITY_ID to DUNEDIN_CITY_ID
    )) as src
    """.format(lion_poi_imsis=conf.LION_POI_IMSIS,
        connections=conf.CONNECTION_DATA, cell_info=conf.CELL_LOCATION,
        region_grid12=conf.REGION_GRID12)
    cur_rem.execute(sql, (start_date_str, end_date_str))
    con_rem.commit()
    sql_add_idx_tpl = """\
    CREATE INDEX {fldname} ON {lion_poi_imsis} ({fldname})
    """
    for fldname in ['IMSI_rti', ]:
        sql = sql_add_idx_tpl.format(lion_poi_imsis=conf.LION_POI_IMSIS,
            fldname=fldname)
        try:
            cur_rem.execute(sql)
        except pg.ProgrammingError:
            con_rem.commit()
    con_rem.commit()
    ## Give public read permissions
    sql_permissions = """\
    GRANT SELECT ON TABLE {lion_poi_imsis} TO public
    """.format(lion_poi_imsis=conf.LION_POI_IMSIS)
    cur_rem.execute(sql_permissions)
    con_rem.commit()
    print("Finished making '{lion_poi_imsis}'".format(
        lion_poi_imsis=conf.LION_POI_IMSIS))

def make_seen_in_day_tbl(start_date_str, end_date_str):
    debug = False
    con_rem, cur_rem = db.Pg.get_rem()
    db.Pg.drop_tbl(con_rem, cur_rem, tbl=conf.SEEN_IN_DAY_LIONS)
    sql_make_tbl = """\
    CREATE TABLE {seen_in_day} (
      IMSI_rti bigint,
      date text,
      region_id integer,
      first_event_region integer
    )
    """.format(seen_in_day=conf.SEEN_IN_DAY_LIONS)
    cur_rem.execute(sql_make_tbl)
    con_rem.commit()
    sql_add_idx_tpl = """\
    CREATE INDEX {fldname} ON {seen_in_day} ({fldname})
    """
    for fldname in ['IMSI_rti', 'date', 'region_id']:
        sql = sql_add_idx_tpl.format(seen_in_day=conf.SEEN_IN_DAY_LIONS,
            fldname=fldname)
        try:
            cur_rem.execute(sql)
        except pg.ProgrammingError:
            con_rem.commit()
    con_rem.commit()
    print("Started making '{seen_in_day}'".format(
        seen_in_day=conf.SEEN_IN_DAY_LIONS))
    ## Give public read permissions
    sql_permissions = """\
    GRANT SELECT ON TABLE {seen_in_day} TO public
    """.format(seen_in_day=conf.SEEN_IN_DAY_LIONS)
    cur_rem.execute(sql_permissions)
    con_rem.commit()
    print("Added permissions to '{seen_in_day}'".format(
        seen_in_day=conf.SEEN_IN_DAY_LIONS))
    sql_tpl = """\
    INSERT
    INTO {seen_in_day}
    SELECT *
    FROM (

      SELECT
      IMSI_rti,
      date,
      region_id,
        MIN(first_event_grid12) AS  -- needing to get a single record per IMSI per region on date but need to be able to identify the first one (the one they woke up in)
      first_event_region

      FROM (
        SELECT
        IMSI_rti,
        date,
        grid12,
          MIN({connections}.start_date) AS  -- we don't care if multiple connections to the same cell_id_rti happen or to multiple cell_id_rti's within a grid - seen at all is what we care about
        first_event_grid12
        FROM {connections}
        INNER JOIN
        {cell_info}
        USING (cell_id_rti)
        INNER JOIN
        {lion_poi_imsis}
        USING(IMSI_rti)
        WHERE date = %s
        AND {cell_info}.start_date <= %s
        AND (
          ({cell_info}.end_date IS NULL)
          OR
          ({cell_info}.end_date >= %s)
        )
        GROUP BY IMSI_rti, date, grid12
      ) AS imsi_date_grid12

      INNER JOIN

      (
        SELECT grid12, region_id
        FROM {region_grid12}
        INNER JOIN
        {region}
        USING (region_id)
        WHERE region_set_id = 2  -- we can rely on these being mutually exclusive so each grid12 will only map to one region_id
      ) AS grid12_regions

      USING(grid12)

      GROUP BY IMSI_rti, date, region_id

    ) AS src
    """.format(seen_in_day=conf.SEEN_IN_DAY_LIONS,
        connections=conf.CONNECTION_DATA, cell_info=conf.CELL_LOCATION,
        lion_poi_imsis=conf.LION_POI_IMSIS, region_grid12=conf.REGION_GRID12,
        region=conf.REGION)
    on_date = datetime.strptime(start_date_str, '%Y-%m-%d')
    end_date = datetime.strptime(end_date_str, '%Y-%m-%d')
    while on_date <= end_date:
        on_date_str = on_date.strftime('%Y-%m-%d')
        cur_rem.execute(sql_tpl, (on_date_str, on_date_str, on_date_str))
        if debug:
            print(str(cur_rem.query, encoding='utf-8'))
            break
        con_rem.commit()
        print("Added seen_in_day data for {}".format(on_date_str))
        on_date += timedelta(days=1)
    print("Finished making '{seen_in_day}'".format(
        seen_in_day=conf.SEEN_IN_DAY_LIONS))

def make_intl_home_loc_tbl():
    """
    http://mcc-mnc.com/
    """
    con_rem, cur_rem = db.Pg.get_rem()
    db.Pg.drop_tbl(con_rem, cur_rem, tbl=conf.LIONS_HOME_INTL)
    sql = """\
    SELECT *
    INTO {home_intl}
    FROM (
      SELECT IMSI_rti,
        CASE
          WHEN SUBSTRING(mccmnc,1,3) = '234' THEN 'United Kingdom'
          WHEN SUBSTRING(mccmnc,1,3) = '505' THEN 'Australia'
          WHEN SUBSTRING(mccmnc,1,3) = '460' THEN 'China'
          WHEN SUBSTRING(mccmnc,1,3) IN ('310', '311', '312', '316') THEN 'USA'
          WHEN SUBSTRING(mccmnc,1,3) = '204' THEN 'Netherlands'
          ELSE 'Other International'
        END AS
      origin
      FROM {lion_poi_imsis}
      INNER JOIN
      {subscribers}
      USING(IMSI_rti)
      WHERE SUBSTRING(mccmnc,1,3) != '530'  -- NZ i.e. domestic
      ORDER BY IMSI_rti
    ) AS src
    """.format(lion_poi_imsis=conf.LION_POI_IMSIS, subscribers=conf.SUBSCRIBERS,
        home_intl=conf.LIONS_HOME_INTL)
    cur_rem.execute(sql)
    con_rem.commit()
    sql_add_idx_tpl = """\
    CREATE INDEX {fldname} ON {home_intl} ({fldname})
    """
    for fldname in ['IMSI_rti', 'origin']:
        sql = sql_add_idx_tpl.format(home_intl=conf.LIONS_HOME_INTL,
            fldname=fldname)
        try:
            cur_rem.execute(sql)
        except pg.ProgrammingError:
            con_rem.commit()
    con_rem.commit()
    ## Give public read permissions
    sql_permissions = """\
    GRANT SELECT ON TABLE {home_intl} TO public
    """.format(home_intl=conf.LIONS_HOME_INTL)
    cur_rem.execute(sql_permissions)
    con_rem.commit()
    print("Finished making '{home_intl}'".format(
        home_intl=conf.LIONS_HOME_INTL))

def make_dom_home_locs_tbl():
    """
    Need first_in_day by IMSI and region for each date for IMSIs NOT in
    intl_home. Look at multiple days and get most common. That's the deemed
    domestic home location.

    Should be looking at previous 35 days and should be a monthly value.
    """
    con_rem, cur_rem = db.Pg.get_rem()
    db.Pg.drop_tbl(con_rem, cur_rem, tbl=conf.LIONS_HOME_DOM)
    sql = """\
    SELECT
    IMSI_rti,
      label AS
    region_name
    INTO {home_dom}
    FROM (
      SELECT DISTINCT ON (IMSI_rti)
      IMSI_rti,
      region_id
      FROM (
        SELECT IMSI_rti,
        region_id,
          COUNT(*) AS
        freq
        FROM (
          SELECT DISTINCT ON (IMSI_rti, date)
          IMSI_rti,
          date,
          region_id
          FROM {seen_in_day_for_lion_poi_imsis} AS seen
          LEFT JOIN
          {home_intl}
          USING(IMSI_rti)
          WHERE {home_intl}.IMSI_rti IS NULL  -- only interested in domestic i.e. not already international
          ORDER BY IMSI_rti, date, first_event_region ASC  -- using the region with the earliest unix epoch integer
        ) AS first_in_day
        GROUP BY IMSI_rti, region_id
      ) AS first_in_day_region_freq
      ORDER BY IMSI_rti, freq DESC, region_id  -- want most commonly woken-up-in region as domestic home location
    ) AS src
    INNER JOIN
    {region}
    USING(region_id)
    """.format(home_dom=conf.LIONS_HOME_DOM, home_intl=conf.LIONS_HOME_INTL,
        seen_in_day_for_lion_poi_imsis=conf.SEEN_IN_DAY_LIONS,
        region=conf.REGION)
    cur_rem.execute(sql)
    con_rem.commit()
    sql_add_idx_tpl = """\
    CREATE INDEX {fldname} ON {home_dom} ({fldname})
    """
    for fldname in ['IMSI_rti', ]:
        sql = sql_add_idx_tpl.format(home_dom=conf.LIONS_HOME_DOM,
            fldname=fldname)
        try:
            cur_rem.execute(sql)
        except pg.ProgrammingError:
            con_rem.commit()
    con_rem.commit()
    ## Give public read permissions
    sql_permissions = """\
    GRANT SELECT ON TABLE {home_dom} TO public
    """.format(home_dom=conf.LIONS_HOME_DOM)
    cur_rem.execute(sql_permissions)
    con_rem.commit()
    print("Finished making '{home_dom}'".format(
        home_dom=conf.LIONS_HOME_DOM))

def make_home_origin_tbl():
    con_rem, cur_rem = db.Pg.get_rem()
    db.Pg.drop_tbl(con_rem, cur_rem, tbl=conf.LIONS_HOME_ORIGIN)
    sql = """\
    SELECT *
    INTO {home_origin}
    FROM (
      SELECT IMSI_rti, origin
      FROM {home_intl}
      UNION
      SELECT IMSI_rti, region_name
      FROM {home_dom}
    ) AS src
    """.format(home_origin=conf.LIONS_HOME_ORIGIN,
        home_intl=conf.LIONS_HOME_INTL, home_dom=conf.LIONS_HOME_DOM)
    cur_rem.execute(sql)
    con_rem.commit()
    sql_add_idx_tpl = """\
    CREATE INDEX {fldname} ON {home_origin} ({fldname})
    """
    for fldname in ['IMSI_rti', ]:
        sql = sql_add_idx_tpl.format(home_origin=conf.LIONS_HOME_ORIGIN,
            fldname=fldname)
        try:
            cur_rem.execute(sql)
        except pg.ProgrammingError:
            con_rem.commit()
    con_rem.commit()
    ## Give public read permissions
    sql_permissions = """\
    GRANT SELECT ON TABLE {home_origin} TO public
    """.format(home_origin=conf.LIONS_HOME_ORIGIN)
    cur_rem.execute(sql_permissions)
    con_rem.commit()
    print("Finished making '{home_origin}'".format(
        home_origin=conf.LIONS_HOME_ORIGIN))

def make_visits_tbl():
    con_rem, cur_rem = db.Pg.get_rem()
    db.Pg.drop_tbl(con_rem, cur_rem, tbl=conf.LIONS_VISITS)
    sql = """\
    SELECT *
      INTO {visits}
      FROM (
      SELECT
      date,
      IMSI_rti,
      dest_region,
        origin AS
      home_origin
      FROM (
        SELECT IMSI_rti,
        date,
          label AS
        dest_region
        FROM {seen_in_day_for_lion_poi_imsis}
        INNER JOIN
        {region}
        USING (region_id)
      ) AS src
      INNER JOIN
      {home_origin}
      USING (IMSI_rti)
      WHERE origin != dest_region
      ORDER BY date, IMSI_rti, dest_region
    ) AS src
    """.format(visits=conf.LIONS_VISITS,
        seen_in_day_for_lion_poi_imsis=conf.SEEN_IN_DAY_LIONS,
        region=conf.REGION, home_origin=conf.LIONS_HOME_ORIGIN)
    cur_rem.execute(sql)
    con_rem.commit()
    sql_add_idx_tpl = """\
    CREATE INDEX {fldname} ON {visits} ({fldname})
    """
    for fldname in ['IMSI_rti', ]:
        sql = sql_add_idx_tpl.format(visits=conf.LIONS_VISITS,
            fldname=fldname)
        try:
            cur_rem.execute(sql)
        except pg.ProgrammingError:
            con_rem.commit()
    con_rem.commit()
    ## Give public read permissions
    sql_permissions = """\
    GRANT SELECT ON TABLE {visits} TO public
    """.format(visits=conf.LIONS_VISITS)
    cur_rem.execute(sql_permissions)
    con_rem.commit()
    print("Finished making '{visits}'".format(visits=conf.LIONS_VISITS))

def visits_data():
    debug = False
    unused_con_rem, cur_rem = db.Pg.get_rem()
    region_names_clause = db.Pg.strs2clause(strs=region_names.values())
    sql = """\
    SELECT
    date,
    dest_region,
    home_origin,
      COUNT(*) AS
    freq
    FROM {visits}
    WHERE dest_region IN {region_names_clause}
    GROUP BY date, dest_region, home_origin
    ORDER BY date, dest_region, COUNT(*) DESC
    """.format(visits=conf.LIONS_VISITS,
        region_names_clause=region_names_clause)
    cur_rem.execute(sql)
    data = cur_rem.fetchall()
    with open(VISITS_CSV, 'w') as f:
        f.write("date,dest_region,home_origin,freq")
        for row in data:
            if debug: print(row)
            f.write("\n" + ",".join(str(x) for x in row))
    print("Finished making {}".format(VISITS_CSV))

def int_only_report():
    debug = True
    verbose = False
    with open(REGIONAL_DATA_DETAILED, 'rb') as f:
        region_daily_dets = pickle.load(f)
    if debug and verbose: pp(region_daily_dets)
    html = []
    for region_id, years_daily_dets in region_daily_dets.items():
        regname = region_names[region_id]
        if debug: print("Processing {}".format(regname))
        for daily_dets in years_daily_dets:
            year = daily_dets[0]['date'][:4]
            if debug: print("Year: {}".format(year))
            xs = [datetime.strptime(x['date'], '%Y-%m-%d') for x in daily_dets]
            ## Domestic
            fig = plt.figure(figsize=(30, 12))
            ax = fig.add_subplot(111)
            title = "{} - {} - {}".format(regname, year, 'domestic')
            ax.set_title(title, fontsize=32, fontweight='bold',
                loc='left', verticalalignment='bottom')
            y_dom = [x['Domestic'] for x in daily_dets]
            ax.bar(xs, y_dom, 0.9, color='grey')
            img_fpath = "images/{}.png".format(utils.Text.filenamify(title))
            fig.savefig(img_fpath, bbox_inches='tight')
            plt.close(fig)
            html.append("<img width=800 padding=0 margin=0 src='{}'>".format(
                img_fpath))
            ## International
            fig = plt.figure(figsize=(30, 12))
            ax = fig.add_subplot(111)
            title = "{} - {} - {}".format(regname, year, 'international')
            ax.set_title(title, fontsize=32, fontweight='bold',
                loc='left', verticalalignment='bottom')
            countries = ['Australia', 'United Kingdom', 'China', 'Netherlands',
                'Other International']
            cumulative_ys = [0 for unused in xs]
            chart_colours = colours()
            for country in countries:
                color=next(chart_colours)
                ys = [x[country] for x in daily_dets]
                bottom = cumulative_ys
                ax.bar(xs, ys, width=0.9, color=color, bottom=bottom, label=country)
                cumulative_ys = [a+b for a, b in zip(ys, cumulative_ys)]
            plt.legend()
            img_fpath = "images/{}.png".format(utils.Text.filenamify(title))
            html.append("<img width=800 padding=0 margin=0 src='{}'>".format(
                img_fpath))
            fig.savefig(img_fpath, bbox_inches='tight')
            plt.close(fig)
    ## assemble
    content = "\n".join(html)
    fpath = ("/home/gps/Documents/tourism/rugby/game_impact.html")
    with open(fpath, 'w') as f:
        f.write(content)
    open_new_tab("file://{}".format(fpath))
    print("Finished!")

def make_report():
    debug = True
    verbose = False
    region_dets = OrderedDict()
    with open(VISITS_CSV) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            dest = row['dest_region']
            on_date = row['date']
            origin = row['home_origin']
            freq = row['freq']
            if dest in region_dets:
                if on_date in region_dets[dest]:
                    region_dets[dest][on_date].append((origin, freq))
                else:
                    region_dets[dest][on_date] = [(origin, freq), ]
            else:
                region_dets[dest] = OrderedDict([
                    (on_date, [(origin, freq), ])
                ])
    html = []
    for region_id in RUGBY_REGIONS:
        regname = region_names[region_id]
        if debug: print("Processing {}".format(regname))
        regdets = region_dets[regname]
        xs = [datetime.strptime(on_date, '%Y-%m-%d')
            for on_date in regdets.keys()]
        if debug and verbose: print(xs)
        ## Domestic
        chart_colours = colours()
        fig = plt.figure(figsize=(30, 12))
        ax = fig.add_subplot(111)
        title = "{} - {}".format(regname,
            'domestic (excluding surrounding region)')
        ax.set_title(title, fontsize=32, fontweight='bold', loc='left',
            verticalalignment='bottom')

        reg_freq_dets = []
        for dom_origin in region_set_2_regnames:
            if dom_origin in (regname, regname_exclusions[regname]):
                continue
            ys = []
            for unused, data in regdets.items():
                freq2use = 0
                for origin, freq in data:
                    if origin == dom_origin:
                        freq2use = int(freq)
                        break
                ys.append(freq2use)
            if debug and verbose: print(ys)
            if sum(ys) == 0:
                continue
            reg_freq_dets.append(
                {'label': dom_origin, 'ys': ys, 'tot': sum(ys)})
        reg_freq_dets = sorted(reg_freq_dets,
            key=lambda freq_dets: freq_dets['tot'], reverse=True)
        ## consolidate all other than top 4
        consol_reg_freq_dets = reg_freq_dets[:4]
        others_freq_dets = reg_freq_dets[4:]
        comb_ys = [0 for unused in xs]
        for other_freq_dets in others_freq_dets:
            comb_ys = [a + b for a, b in zip(other_freq_dets['ys'], comb_ys)]
        consol_reg_freq_dets.append(
            {'label': 'Other regions (ex surrounding)', 'ys': comb_ys,
             'tot': sum(comb_ys)})

        cumulative_ys = [0 for unused in xs]
        for freq_dets in consol_reg_freq_dets:
            dom_origin = freq_dets['label']
            ys = freq_dets['ys']
            if dom_origin in (regname, regname_exclusions[regname]):
                continue
            color=next(chart_colours)
            bottom = cumulative_ys
            ax.bar(xs, ys, 0.9, color=color, bottom=bottom, label=dom_origin)
            cumulative_ys = [a+b for a, b in zip(ys, cumulative_ys)]
            plt.legend()
        img_fpath = "images/{}.png".format(utils.Text.filenamify(title))
        fig.savefig(img_fpath, bbox_inches='tight')
        plt.close(fig)
        html.append("<img width=800 padding=0 margin=0 src='{}'>".format(
            img_fpath))
        ## International
        chart_colours = colours()
        fig = plt.figure(figsize=(30, 12))
        ax = fig.add_subplot(111)
        title = "{} - {}".format(regname, 'international')
        ax.set_title(title, fontsize=32, fontweight='bold',
            loc='left', verticalalignment='bottom')
        cumulative_ys = [0 for unused in xs]
        countries = ['Australia', 'United Kingdom', 'China', 'USA',
            'Netherlands', 'Other International']
        for country in countries:
            color=next(chart_colours)
            ys = []
            for unused, data in regdets.items():
                freq2use = 0
                for origin, freq in data:
                    if origin == country:
                        freq2use = int(freq)
                        break
                ys.append(freq2use)
            if debug and verbose: print(ys)
            bottom = cumulative_ys
            ax.bar(xs, ys, 0.9, color=color, bottom=bottom, label=country)
            cumulative_ys = [a+b for a, b in zip(ys, cumulative_ys)]
            plt.legend()
        img_fpath = "images/{}.png".format(utils.Text.filenamify(title))
        fig.savefig(img_fpath, bbox_inches='tight')
        plt.close(fig)
        html.append("<img width=800 padding=0 margin=0 src='{}'>".format(
            img_fpath))
    ## assemble
    content = "\n".join(html)
    fpath = ("/home/gps/Documents/tourism/rugby/lions_visits.html")
    with open(fpath, 'w') as f:
        f.write(content)
    open_new_tab("file://{}".format(fpath))
    print("Finished!")
            

def main():
    make_special_regions_src = False
    make_rugby_region_set_src = False
    make_reused_regions_src = False
    make_complementary_regions_src = False
    make_lions_poi_imsis_src = False
    make_seen_in_day = False
    make_intl_home_locs_src = False
    make_dom_home_locs_src = False
    make_home_origin_src = False
    make_visits_src = False
    display_visits_data = False

    unused_con_local, cur_local, unused_cur_local2 = db.Pg.get_local()
    if make_special_regions_src:
        add_special_regions(cur_local)
    if make_rugby_region_set_src:
        add_rugby_region_set()
    if make_reused_regions_src:
        reuse_old_regions()
    if make_complementary_regions_src:
        add_complementary_regions()
    start_date_str = '2017-05-01'
    end_date_str = '2017-07-10'
    if make_lions_poi_imsis_src:
        make_lions_poi_imsis_tbl(start_date_str, end_date_str)
    if make_seen_in_day:
        make_seen_in_day_tbl(start_date_str, end_date_str)
    if make_intl_home_locs_src:
        make_intl_home_loc_tbl()
    if make_dom_home_locs_src:
        make_dom_home_locs_tbl()
    if make_home_origin_src:
        make_home_origin_tbl()
    if make_visits_src:
        make_visits_tbl()
    if display_visits_data:
        visits_data()
    make_report()


if __name__ == '__main__':
    main()
